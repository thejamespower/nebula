nebulaEditor.controller('Viewer', ['$scope', '$http', '$interval', function($scope, $http, $interval) {

  // $scope.url = 'https://nebula-editor.herokuapp.com'; // Url for live deployment
  $scope.url = ''; // Url for local deployment

  // Initaed update count to 0
  $scope.updated = 0;

  $scope.getContent = function() {

    // Get the package json
    $http.get($scope.url + '/getJson')
      .then(function(response) {
        // On promise complete, store json in the controller config property
        $scope.config = response.data;
      });

    // Get HTML content
    $http.get($scope.url + '/getHtml')
      .then(function(response) {
        // On promise complete, store html in the controller loadedHtml property
        $scope.loadedHtml = response.data;
        // htmlContent and loadedHtml have separate variables so that data can be passed back from the Angular view
        $scope.htmlViewer = $scope.loadedHtml;
      });

    $scope.updated++;
  };
  $scope.getContent();

  // Check for new content every 1 second
  $interval($scope.getContent, 1000);

}]);
