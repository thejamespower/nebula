nebulaEditor.controller('Editor', ['$scope', '$http', function($scope, $http) {

  // $scope.url = 'https://nebula-editor.herokuapp.com'; // Url for live deployment
  $scope.url = ''; // Url for local deployment

  // Initialise edited no. to 0
  $scope.edited = 0;

  // Initialise config obect
  $scope.config = {};

  $scope.watchJson = function() {
    // Edit function cannot be used on X-editable directive, so a watch function is used
    $scope.$watch('config', function(newValue, oldValue) {
      if (newValue != oldValue) {
        // Increase edited count
        $scope.edited++;
        // Send Json content - RESTful post to Express
        $http.post($scope.url + '/sendJson', newValue, {
          'headers': {
            // Http header for json
            'Content-Type': 'application/json'
          }
        });
      }
    }, true);
  };

  // Fires from view when user edits TextAngular directive content
  $scope.edit = function() {
    // Increase edited count
    $scope.edited++;
    // Send HTML content - RESTful post to Express
    $http.post($scope.url + '/sendHtml', $scope.htmlContent, {
      'headers': {
        // Http header for html
        'Content-Type': 'text/html'
      }
    });
  };

  // Get the package json - RESTful get to Express
  $http.get($scope.url + '/getJson')
    .then(function(response) {
      // On promise complete, store json in the controller config property
      $scope.config = response.data;

      $scope.watchJson();
    });

  // Get HTML content - RESTful get to Express
  $http.get($scope.url + '/getHtml')
    .then(function(response) {
      // On promise complete, store html in the controller loadedHtml property
      $scope.loadedHtml = response.data;
      // htmlContent and loadedHtml have separate variables so that data can be passed back from the Angular view
      $scope.htmlContent = $scope.loadedHtml;
    });

  // Toggle Raw HTML view
  $('.rawHtml').hide();
  $scope.toggleRaw = function() {
    $('.rawHtml').toggle();
  };

}]);
