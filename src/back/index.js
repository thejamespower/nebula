var express = require('express'),
  app = express(),
  fs = require('fs'),
  bodyParser = require('body-parser'),

  // port = process.env.PORT || 8080; // Heroku only
  port = 3000; // Local (nodemon) only test

// Serve static files
app.use(express.static('public'));

// Express needs bodyParser to parse json
app.use(bodyParser.json());

// Express needs bodyParser to parse html
app.use(bodyParser.text({
  'type': 'text/html'
}));

// All default get http requests redirect to app page
app.get('/', function(req, res) {
  res.sendFile('public/index.html', {
    root: './'
  });
});

// Get requests to viewer page get a redirect to Angular Router
app.get('/viewer', function(req, res) {
  res.redirect(301, '/#/viewer'); // Redirect
});

// RESTful http get request for getting json data (course properties)
app.get('/getJson', function(req, res) {
  // Read json file from filesystem synchronously
  var jsonBuff = fs.readFileSync('./service/content.json'),
    // Parse json buffer into json
    json = JSON.parse(jsonBuff);
  // Send json to client
  res.status(200).send(json); // Okay
});

// RESTful http get request for getting html content (course content)
app.get('/getHtml', function(req, res) {
  // Read html file from filesystem synchronously
  var html = fs.readFileSync('./service/content.html');
  // Send html to client, buffer is parsed automatically by bodyParser
  res.status(200).send(html); // Okay
});

// RESTful http post request for sending html content (course content from Angular view)
app.post('/sendHtml', function(req, res) {
  // Request body contains the data we want
  var posted = req.body;
  // Write req.body (html) to the content.html file
  fs.writeFile('./service/content.html', posted, function(err) {
    if (err) {
      // If error log error
      return console.log(err);
    } else {
      // Return okay status and tell client what was posted
      res.status(200) // Okay
      res.setHeader('Content-Type', 'text/plain')
      res.write('You posted:\n')
      res.end(posted)
    }
  });
});

// RESTful http post request for sending json content (course properties from Angular view)
app.post('/sendJson', function(req, res) {

  // Request body contains the data we want
  // We need to parse the object into JSON string
  var posted = JSON.stringify(req.body);
  // Write req.body(html) to the content.html file
  fs.writeFile('./service/content.json', posted, function(err) {
    if (err) {
      // If error log error
      return console.log(err);
    } else {
      // Return okay status and tell client what was posted
      res.status(200) // Okay
      res.setHeader('Content-Type', 'text/plain')
      res.write('You posted:\n')
      res.end(posted);
    }
  });
});

app.listen(port, function() {
  console.log('Nebula server listening on port:' + port);
});
